<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mthome');

/** MySQL database username */
define('DB_USER', 'mthome');

/** MySQL database password */
define('DB_PASSWORD', 'Abcd@123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9?*#INCNt0jEWDqin.<.{].!u:J0Nz`zA>9tP>D]$N/a}ea ^9p+BdS>^968rdjf');
define('SECURE_AUTH_KEY',  'yq4I+^f;O,S//H*}NUPNe@AHY$u]L|}/>Ks Q?+=kd+^EZYLop@s}YgQ4gjsv|BI');
define('LOGGED_IN_KEY',    'OtNav]`vI2~n$5UdUy7`xL-{N3e>+j/$y<Rr&)?;?wikOIrXR:qq$}o7*e~x~L?<');
define('NONCE_KEY',        'GIHx TOmE/GY{*:h[CWy3i2v}=w>W@r2qYslAVeY9<NV%qTr-DNL!LbGPE8g]7:7');
define('AUTH_SALT',        'H;KC**X`.#k dQKU?pCtejm!%H ag.Lr.7&6X[@0Zad>3=Bfly<DO^Kx0pM9d_;P');
define('SECURE_AUTH_SALT', 'q~T`vhNxmtOs<g_+`@&mK~)<Kn]Ux.bh_B1%r+shE:R-{!IZ(_ocGqvzDI}~1WJ6');
define('LOGGED_IN_SALT',   '26=P$793^:]KW;@cV|/2)lP(bZg$a#2pN^dD!R;SN5gT(3?_J`<&*^/@V[cX^|_t');
define('NONCE_SALT',       '}w~EHx:@)kFgx]gun5Ey?@qxcp_V^C<CpR) Qbv2{W7FSkB] K6,_a(f0p0jV?mU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'WP_DEBUG_LOG', false );
define( 'WP_DEBUG_DISPLAY', false );
define('FS_METHOD', 'direct');
define('WP_UPLOAD_MAX_FILESIZE', '128M');
define('WP_MEMORY_LIMIT', '256M');
@ini_set( 'upload_max_filesize' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'memory_limit', '256M' );
@ini_set( 'max_execution_time', '300' );
@ini_set( 'max_input_time', '300' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
